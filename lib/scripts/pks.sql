SELECT CASE tc.table_schema WHEN 'public' THEN tc.table_name::regclass ELSE (tc.table_schema || '.' || tc.table_name)::regclass END AS table_name,
  kc.column_name, kc.ordinal_position AS column_index
FROM information_schema.table_constraints tc
JOIN information_schema.key_column_usage kc
  ON kc.table_name = tc.table_name
  AND kc.constraint_schema = tc.table_schema
  AND kc.constraint_name = tc.constraint_name
WHERE tc.constraint_type = 'PRIMARY KEY';
