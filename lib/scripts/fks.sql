SELECT CASE source.table_schema WHEN 'public' THEN source.table_name::regclass ELSE (source.table_schema || '.' || source.table_name)::regclass END AS source_table,
  source.column_name AS source_column,
  CASE target.table_schema WHEN 'public' THEN target.table_name::regclass ELSE (target.table_schema || '.' || target.table_name)::regclass END AS target_table,
  target.column_name AS target_column
FROM information_schema.referential_constraints rc
JOIN information_schema.key_column_usage AS source
  ON source.constraint_catalog = rc.constraint_catalog
  AND source.constraint_schema = rc.constraint_schema
  AND source.constraint_name = rc.constraint_name
JOIN information_schema.key_column_usage AS target
  ON target.constraint_catalog = rc.unique_constraint_catalog
  AND target.constraint_schema = rc.unique_constraint_schema
  AND target.constraint_name = rc.unique_constraint_name
ORDER BY source.table_schema, source.table_name, source.column_name;
