'use strict';

const _ = require('lodash');

exports = module.exports = (a, b, strict=false) => {
  const aKeys = Object.keys(a);
  const bKeys = Object.keys(b);

  return _.isEqual(aKeys, bKeys) && aKeys.every(key => {
    if (strict) {
      return _.difference(a[key], b[key]).length === 0;
    }

    return _.intersection(a[key], b[key]).length > 0;
  });
};
