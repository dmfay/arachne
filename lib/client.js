'use strict';

const co = require('co');
const fs = require('mz/fs');
const pg = require('pg');

const scriptLocation = `${__dirname}/scripts`;

const Client = function (config) {
  const pool = new pg.Pool(config);

  this.run = (query, params) => new Promise((resolve, reject) => {
    pool.connect((err, client, done) => {
      if (err) { return reject(err); }

      client.query(query, params, (err, result) => {
        done();

        if (err) { return reject(err); }

        return resolve(result);
      });
    });
  });

  return this;
};

Client.prototype.query = co.wrap(function* (table, condition) {
  const params = [];
  const predicates = Object.keys(condition).map(k => {
    params.push(condition[k]);

    if (Array.isArray(condition[k])) {
      return `${k} = ANY ($${params.length})`;
    } else {
      return `${k} = $${params.length}`;
    }
  });

  return yield this.run(`SELECT * FROM ${table} WHERE ${predicates.join(' AND ')}`, params);
});

Client.prototype.script = co.wrap(function* (filename) {
  const sql = yield fs.readFile(`${scriptLocation}/${filename}.sql`);

  const result = yield this.run(sql.toString());

  return result.rows;
});

exports = module.exports = Client;
