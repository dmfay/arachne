'use strict';

const _ = require('lodash');
const conditionsOverlap = require('./conditions-overlap');

exports = module.exports = (a, b, strict=false) => {
  const p1 = ['source_table', 'source_column', 'source_condition'];
  const p2 = ['target_table', 'target_column', 'target_condition'];
  const cmp = (a, b) => {
    if (_.isObject(a) && _.isObject(b)) {
      return _.isEqual(Object.keys(a), Object.keys(b));
    }

    return a === b;
  };

  const identical = p1.every(p => cmp(a[p], b[p])) && p2.every(p => cmp(a[p], b[p]));
  const reversed = p1.every((p, i) => cmp(a[p], b[p2[i]])) && p2.every((p, i) => cmp(a[p], b[p1[i]]));

  // order is important with strict: we're looking for anything in a that we
  // don't already know about
  if (identical) {
    return conditionsOverlap(a.source_condition, b.source_condition, strict) && conditionsOverlap(a.target_condition, b.target_condition, strict);
  } else if (reversed) {
    return conditionsOverlap(a.source_condition, b.target_condition, strict) && conditionsOverlap(a.target_condition, b.source_condition, strict);
  } else {
    return false;
  }
};
