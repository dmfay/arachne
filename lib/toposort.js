'use strict';

const conditionsOverlap = require('./conditions-overlap');

exports = module.exports = function* (digraph) {
  while (digraph.vertices.size > 0) {
    // get vertices which do not originate any foreign key relationships
    const vertices = digraph.vertices.filter(v => !digraph.edges.some(e => {
      return e.source_table === v.table && conditionsOverlap(e.source_condition, v.condition, true);
    }));

    // get edges targeting above vertices
    const edges = digraph.edges.filter(e => vertices.some(v => {
      return v.table === e.target_table && conditionsOverlap(e.target_condition, v.condition, true);
    }));

    yield {vertices: vertices, edges: edges};
  }
};
