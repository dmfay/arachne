'use strict';

const _ = require('lodash');

const sanitize = val => {
  if (!_.isString(val)) { return val; }

  val = val.replace(/\n/g, '\\n');
  val = val.replace(/\r/g, '\\r');
  val = val.replace(/\t/g, '\\t');
  val = val.replace(/\\/g, '\\\\');

  return val;
};

const sanitizeForArray = val => {
  if (!_.isString(val)) { return val; }

  val = val.replace(/\\/g, '\\\\\\\\');
  val = val.replace(/"/g, '\\\\"');
  val = val.replace(/'/g, '\\\\\'');
  val = val.replace(/,/g, '\\\\,');
  val = val.replace(/([{}])/g, '\\\\$1');

  return `"${val}"`;
};

const pad = (number, digits) => {
  number = '' + number;
  while (number.length < digits) { number = '0' + number; }
  return number;
};

const convertDate = val => {
  // shamelessly stolen from node-pg source!
  let offset = -val.getTimezoneOffset();
  let ret = pad(val.getFullYear(), 4) + '-' +
    pad(val.getMonth() + 1, 2) + '-' +
    pad(val.getDate(), 2) + 'T' +
    pad(val.getHours(), 2) + ':' +
    pad(val.getMinutes(), 2) + ':' +
    pad(val.getSeconds(), 2) + '.' +
    pad(val.getMilliseconds(), 3);

  if (offset < 0) {
    ret += '-';
    offset *= -1;
  } else {
    ret += '+';
  }

  return `${ret}${pad(Math.floor(offset/60), 2)}:${pad(offset%60, 2)}`;
};

// TODO composing these would be nice
const bytea = val => `E'\\\\x${val}`;
const tbd = val => val;
const array = val => `{${val.map(sanitizeForArray).join(',')}}`;
const date = val => convertDate(val);
const dateArray = val => `{${val.map(convertDate).join(',')}}`;
const stringify = val => `${sanitize(JSON.stringify(val))}`;

const types = {
  17: bytea, // bytea
  114: stringify, // json
  142: tbd, // xml
  143: tbd, // _xml
  199: tbd, // _json
  600: tbd, // point
  601: tbd, // lseg
  602: tbd, // path
  603: tbd, // box
  604: tbd, // polygon
  628: tbd, // line
  629: tbd, // _line
  650: tbd, // cidr
  651: tbd, // _cidr
  702: tbd, // abstime
  703: tbd, // reltime
  704: tbd, // tinterval
  718: tbd, // circle
  719: tbd, // _circle
  1000: array, // _bool
  1001: tbd, // _bytea
  1002: array, // _char
  1005: array, // _int2
  1007: array, // _int4
  1009: array, // _text
  1015: array, // _varchar
  1016: array, // _int8
  1017: tbd, // _point
  1018: tbd, // _lseg
  1019: tbd, // _path
  1020: tbd, // _box
  1021: array, // _float4
  1022: array, // _float8
  1023: tbd, // _abstime
  1024: tbd, // _reltime
  1025: tbd, // _tinterval
  1027: tbd, // _polygon
  1040: array, // _macaddr
  1041: array, // _inet
  1082: date, // date
  1083: date, // time
  1114: date, // timestamp
  1115: dateArray, // _timestamp
  1182: dateArray, // _date
  1183: dateArray, // _time
  1184: date, // timestamptz
  1185: dateArray, // _timestamptz
  1186: tbd, // interval
  1187: tbd, // _interval
  1231: array, // _numeric
  1270: array, // _timetz
  1560: tbd, // bit
  1561: tbd, // _bit
  1562: tbd, // varbit
  1563: tbd, // _varbit
  2951: array, // _uuid
  3802: stringify, // jsonb
  3807: tbd, // _jsonb
  3904: tbd, // int4range
  3905: tbd, // _int4range
  3906: tbd, // numrange
  3907: tbd, // _numrange
  3908: tbd, // tsrange
  3909: tbd, // _tsrange
  3910: tbd, // tstzrange
  3911: tbd, // _tstzrange
  3912: tbd, // daterange
  3913: tbd, // _daterange
  3926: tbd, // int8range
  3927: tbd, // _int8range
};

exports = module.exports = (row, fields) => {
  return fields.map(f => {
    if (row[f.name] === null) { return '\\N'; }

    const formatter = types[f.dataTypeID];
    const value = sanitize(row[f.name]);

    if (formatter) { return formatter(value); }

    return value;
  }).join('\t');
};
