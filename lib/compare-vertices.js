'use strict';

const conditionsOverlap = require('./conditions-overlap');

exports = module.exports = function (a, b) {
  if (a.table !== b.table) { return false; }

  return conditionsOverlap(a.condition, b.condition);
};
