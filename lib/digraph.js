'use strict';

const _ = require('lodash');
const Set2 = require('./set2');

class Digraph {
  constructor(vertices, edges, compareVertices, compareEdges) {
    this.vertices = new Set2(vertices || []);
    this.edges = new Set2(edges || []);
    this.compareVertices = compareVertices || _.isEqual;
    this.compareEdges = compareEdges || _.isEqual;
  }

  merge(that) {
    this.vertices = this.vertices.merge(that.vertices, this.compareVertices);
    this.edges = this.edges.merge(that.edges, this.compareEdges);
  }
}

exports = module.exports = Digraph;
