'use strict';

const _ = require('lodash');

class Set2 extends Set {
  constructor(elements) {
    super(elements);
  }

  filter(predicate) {
    return new Set2([...this].filter(predicate));
  }

  some(predicate) {
    return [...this].some(predicate);
  }

  difference(that) {
    return new Set2([...that].filter(x => !this.has(x)));
  }

  merge(that, comparator=_.isEqual) {
    /*
     * Thanks to arachne's recursive process, the "that" set being merged in is
     * guaranteed to be free from overlaps, since they would have been
     * consolidated as it was being composed in earlier merge steps. So this
     * isn't a generally applicable merge, but it works for what we need it to.
     */
    return new Set2([...this].reduce((set, val) => {
      let current = set.find(v => comparator(v, val));

      if (current) {
        current = _.mergeWith(current, val, (aa, bb) => { if (_.isArray(aa)) { return _.union(aa, bb); }});
      } else {
        set.push(val);
      }

      return set;
    }, [...that]));
  }
}

exports = module.exports = Set2;
