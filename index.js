'use strict';

const _ = require('lodash');
const co = require('co');
const format = require('./lib/format');
const Client = require('./lib/client');
const Digraph = require('./lib/digraph');
const compareVertices = require('./lib/compare-vertices');
const compareEdges = require('./lib/compare-edges');
const toposort = require('./lib/toposort');

exports = module.exports = co.wrap(function* (config, origin, condition, ws) {
  const client = new Client(config);

  const fkeys = yield client.script('fks');
  const pkeys = yield client.script('pks');

  const script = co.wrap(function* (vertex) {
    const resultset = yield client.query(vertex.table, vertex.condition);

    if (resultset.rows.length === 0) { return ''; }

    const columns = resultset.fields.map(f => f.name).join(',');

    ws.write(`COPY ${vertex.table} (${columns}) FROM stdin;\n`);

    resultset.rows.forEach(row => {
      ws.write(format(row, resultset.fields) + '\n');
    });

    ws.write('\\.\n\n');
  });

  condition = _.mapValues(condition, v => {
    if (_.isArray(v)) { return v; }
    return [v];
  });

  const scan = co.wrap(function* (digraph, vertex) {
    const ourResult = yield client.query(vertex.table, vertex.condition);

    return fkeys.reduce((promise, key) => {
      var us, them;

      if (key.source_table === vertex.table) {
        us = 'source';
        them = 'target';
      } else if (key.target_table === vertex.table) {
        us = 'target';
        them = 'source';
      } else {
        return promise;
      }

      const edge = _.cloneDeep(key);
      const ourColumn = key[`${us}_column`];
      const theirTable = key[`${them}_table`];
      const theirColumn = key[`${them}_column`];
      const theirKeyColumns = pkeys.filter(k => k.table_name === theirTable);
      const queryCondition = {};
      queryCondition[theirColumn] = _.uniq(ourResult.rows.map(r => r[ourColumn]));

      return promise.then(co.wrap(function* () {
        // TODO check key columns and only get pks and fks instead of everything
        const theirResult = yield client.query(theirTable, queryCondition);
        let theirCondition = null, newEdgeInfo = false;

        if (theirResult.rows.length > 0) {
          theirCondition = {};

          if (theirKeyColumns.length > 0) {
            theirCondition = theirKeyColumns.reduce((acc, k) => {
              acc[k.column_name] = _(theirResult.rows).map(r => r[k.column_name]).uniq().filter().value();

              return acc;
            }, {});
          } else {
            // junction table without primary key (TODO: test?)
            theirCondition = fkeys.reduce((acc, k) => {
              if (k.target_table === vertex.table) {
                acc[k.source_column] = _(theirResult.rows).map(r => r[k.source_column]).uniq().filter().value();
              }

              return acc;
            }, {});
          }

          edge[`${us}_condition`] = vertex.condition;
          edge[`${them}_condition`] = theirCondition;

          // if we're adding any new edge information, we need to recurse to follow up
          newEdgeInfo = !digraph.edges.some(e => compareEdges(edge, e, true));

          digraph.merge(new Digraph([vertex], [edge]));
        } else {
          // we tried to follow a null foreign key, but still need to add this vertex
          digraph.merge(new Digraph([vertex], []));
        }

        if (newEdgeInfo && !!theirCondition) {
          const foreignGraph = yield scan(digraph, {table: theirTable, condition: theirCondition});

          digraph.merge(foreignGraph);
        }

        return Promise.resolve(digraph);
      }));
    }, Promise.resolve(digraph));
  });

  const vertex = {table: origin, condition: condition};
  const digraph = yield scan(new Digraph([vertex], [], compareVertices, compareEdges), vertex);

  const sort = toposort(digraph);

  for (const subgraph of sort) {
    // if we started a loop iteration with vertices but can't script any of
    // them, the graph is cyclic and we have to abort.
    if (subgraph.vertices.size === 0) { return ''; }

    digraph.vertices = subgraph.vertices.difference(digraph.vertices);
    digraph.edges = subgraph.edges.difference(digraph.edges);

    yield Promise.all([...subgraph.vertices].map(script));
  }
});
