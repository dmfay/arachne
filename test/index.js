'use strict';

const arachne = require('../index');
const assert = require('chai').assert;
const fs = require('mz/fs');
const stream = require('stream');
const Client = require('../lib/client');
const format = require('../lib/format');

require('co-mocha');

describe('arachne', function () {
  const config = {
    host: process.env.POSTGRES_HOST || 'localhost',
    database: 'arachne',
    user: 'postgres'
  };
  const client = new Client(config);
  var output;

  const ws = new stream.Writable({decodeStrings: false});
  ws._write = (chunk, enc, next) => {
    output.push(chunk).toString();

    next();
  };

  before(function* () {
    const schema = yield fs.readFile('./test/schema.sql');

    return yield client.run(schema.toString());
  });

  beforeEach(function() {
    output = [];
  });

  after(function* () {
    return yield client.run('DROP SCHEMA arachne CASCADE;');
  });

  describe('types', function () {
    it('scripts array literals', function* () {
      yield arachne(config, 'arachne.arraytable', {id: 1}, ws);

      assert.equal(output[0], 'COPY arachne.arraytable (id,arr) FROM stdin;\n');
      assert.equal(output[1], '1\t{"one","two"}\n');
      assert.equal(output[2], '\\.\n\n');
    });

    it('escapes array contents', function* () {
      yield client.run('INSERT INTO arachne.arraytable (id, arr) VALUES (2, $1)', [['\\ , \' " { }']]);
      yield arachne(config, 'arachne.arraytable', {id: 2}, ws);

      assert.equal(output[0], 'COPY arachne.arraytable (id,arr) FROM stdin;\n');
      assert.equal(output[1], '2\t{"\\\\\\\\ \\\\, \\\\\' \\\\" \\\\{ \\\\}"}\n');
      assert.equal(output[2], '\\.\n\n');
    });

    it('parses and quotes dates', function* () {
      const date = new Date();
      const dates = [date, new Date(0)];

      yield client.run('INSERT INTO datetable VALUES (1, $1, $2)', [date, dates]);

      yield arachne(config, 'arachne.datetable', {id: 1}, ws);

      const row = format({id: 1, value: date, value_array: dates}, [
        {name: 'id', dataTypeID: 23},
        {name: 'value', dataTypeID: 1184},
        {name: 'value_array', dataTypeID: 1185}
      ]);

      assert.equal(output[0], 'COPY arachne.datetable (id,value,value_array) FROM stdin;\n');
      assert.equal(output[1], `${row}\n`);
      assert.equal(output[2], '\\.\n\n');
    });

    it('processes enum values', function* () {
      yield arachne(config, 'arachne.enumtable', {id: 1}, ws);

      assert.equal(output[0], 'COPY arachne.enumtable (id,value,value_array) FROM stdin;\n');
      assert.equal(output[1], '1\tone\t{one,two,three}\n');
      assert.equal(output[2], '\\.\n\n');
    });
  });

  describe('table with no relations', function () {
    it('generates a script for the source entity', function* () {
      yield arachne(config, 'arachne.alpha', {id: 1}, ws);

      assert.equal(output[0], 'COPY arachne.alpha (id,field) FROM stdin;\n');
      assert.equal(output[1], '1\talpha1\n');
      assert.equal(output[2], '\\.\n\n');
    });

    it('generates a script for multiple source entities', function* () {
      yield arachne(config, 'arachne.alpha', {id: [1, 2]}, ws);

      assert.equal(output[0], 'COPY arachne.alpha (id,field) FROM stdin;\n');
      assert.equal(output[1], '1\talpha1\n');
      assert.equal(output[2], '2\talpha2\n');
      assert.equal(output[3], '\\.\n\n');
    });

    it('works with compound criteria', function* () {
      yield arachne(config, 'arachne.alpha', {id: 1, field: 'alpha1'}, ws);

      assert.equal(output[0], 'COPY arachne.alpha (id,field) FROM stdin;\n');
      assert.equal(output[1], '1\talpha1\n');
      assert.equal(output[2], '\\.\n\n');
    });
  });

  describe('table with foreign keys', function () {
    it('follows foreign keys defined in the source table', function* () {
      yield arachne(config, 'arachne.gamma', {id: 1}, ws);

      assert.equal(output[0], 'COPY arachne.beta (id,field) FROM stdin;\n');
      assert.equal(output[1], '1\tbeta1\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.gamma (id,beta_id,field) FROM stdin;\n');
      assert.equal(output[4], '1\t1\tgamma1\n');
      assert.equal(output[5], '\\.\n\n');
    });

    it('stops if foreign keys are null', function* () {
      yield arachne(config, 'arachne.theta', {id: 3}, ws);

      assert.equal(output[0], 'COPY arachne.theta (id,field) FROM stdin;\n');
      assert.equal(output[1], '3\ttheta3\n');
      assert.equal(output[2], '\\.\n\n');
    });

    it('follows compound foreign keys defined in the source table', function* () {

    });

    it('follows self joining tables', function* () {
      yield arachne(config, 'arachne.phi', {id: 3}, ws);

      assert.equal(output[0], 'COPY arachne.phi (id,phi_id,field) FROM stdin;\n');
      assert.equal(output[1], '1\t\\N\tphi1\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.phi (id,phi_id,field) FROM stdin;\n');
      assert.equal(output[4], '2\t1\tphi2\n');
      assert.equal(output[5], '\\.\n\n');
      assert.equal(output[6], 'COPY arachne.phi (id,phi_id,field) FROM stdin;\n');
      assert.equal(output[7], '3\t2\tphi3\n');
      assert.equal(output[8], '\\.\n\n');
    });
  });

  describe('table targeted by foreign keys', function () {
    it('looks for foreign keys pointing to the source table', function* () {
      yield arachne(config, 'arachne.beta', {id: 1}, ws);

      assert.equal(output[0], 'COPY arachne.beta (id,field) FROM stdin;\n');
      assert.equal(output[1], '1\tbeta1\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.gamma (id,beta_id,field) FROM stdin;\n');
      assert.equal(output[4], '1\t1\tgamma1\n');
      assert.equal(output[5], '\\.\n\n');
    });

    it('looks for compound foreign keys pointing to the source table', function* () {

    });
  });

  describe('many:many relationships', function () {
    it('scripts many:many relationships from a source table', function* () {
      yield arachne(config, 'arachne.theta', {id: 1}, ws);

      assert.equal(output[0], 'COPY arachne.iota (id,field) FROM stdin;\n');
      assert.equal(output[1], '1\tiota1\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.theta (id,field) FROM stdin;\n');
      assert.equal(output[4], '1\ttheta1\n');
      assert.equal(output[5], '\\.\n\n');
      assert.equal(output[6], 'COPY arachne.kappa (theta_id,iota_id,field) FROM stdin;\n');
      assert.equal(output[7], '1\t1\tkappa1\n');
      assert.equal(output[8], '\\.\n\n');
    });

    it('scripts many:many relationships from the junction table', function* () {
      yield arachne(config, 'arachne.kappa', {theta_id: 1, iota_id: 1}, ws);

      assert.equal(output[0], 'COPY arachne.theta (id,field) FROM stdin;\n');
      assert.equal(output[1], '1\ttheta1\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.iota (id,field) FROM stdin;\n');
      assert.equal(output[4], '1\tiota1\n');
      assert.equal(output[5], '\\.\n\n');
      assert.equal(output[6], 'COPY arachne.kappa (theta_id,iota_id,field) FROM stdin;\n');
      assert.equal(output[7], '1\t1\tkappa1\n');
      assert.equal(output[8], '\\.\n\n');
    });

    it('scripts many:many relationships starting with incomplete information', function* () {
      yield arachne(config, 'arachne.iota', {id: 2}, ws);

      assert.equal(output[0], 'COPY arachne.iota (id,field) FROM stdin;\n');
      assert.equal(output[1], '2\tiota2\n');
      assert.equal(output[2], '3\tiota3\n');
      assert.equal(output[3], '\\.\n\n');
      assert.equal(output[4], 'COPY arachne.theta (id,field) FROM stdin;\n');
      assert.equal(output[5], '2\ttheta2\n');
      assert.equal(output[6], '\\.\n\n');
      assert.equal(output[7], 'COPY arachne.kappa (theta_id,iota_id,field) FROM stdin;\n');
      assert.equal(output[8], '2\t2\tkappa2\n');
      assert.equal(output[9], '2\t3\tkappa3\n');
      assert.equal(output[10], '\\.\n\n');
    });

    it('scripts confluences of many:many relationships', function* () {
      yield arachne(config, 'arachne.rho', {id: 2}, ws);

      assert.equal(output[0], 'COPY arachne.rho (id,field) FROM stdin;\n');
      assert.equal(output[1], '2\trho2\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.upsilon (id,field) FROM stdin;\n');
      assert.equal(output[4], '2\tupsilon2\n');
      assert.equal(output[5], '3\tupsilon3\n');
      assert.equal(output[6], '\\.\n\n');
      assert.equal(output[7], 'COPY arachne.tau (rho_id,upsilon_id,field) FROM stdin;\n');
      assert.equal(output[8], '2\t3\ttau2\n');
      assert.equal(output[9], '\\.\n\n');
      assert.equal(output[10], 'COPY arachne.sigma (rho_id,upsilon_id,field) FROM stdin;\n');
      assert.equal(output[11], '2\t2\tsigma2\n');
      assert.equal(output[12], '2\t3\tsigma3\n');
      assert.equal(output[13], '\\.\n\n');
    });
  });

  describe('cycles', function () {
    it('tracks conditions in vertices and edges to script false cycles', function* () {
      yield arachne(config, 'arachne.zeta', {id: 2}, ws);

      assert.equal(output[0], 'COPY arachne.zeta (id,eta_id,field) FROM stdin;\n');
      assert.equal(output[1], '3\t\\N\tzeta3\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.eta (id,zeta_id,field) FROM stdin;\n');
      assert.equal(output[4], '2\t3\teta2\n');
      assert.equal(output[5], '\\.\n\n');
      assert.equal(output[6], 'COPY arachne.zeta (id,eta_id,field) FROM stdin;\n');
      assert.equal(output[7], '2\t2\tzeta2\n');
      assert.equal(output[8], '\\.\n\n');
    });

    it('avoids circular dependency traps', function* () {
      yield arachne(config, 'arachne.zeta', {id: 1}, ws);

      assert.lengthOf(output, 0);
    });
  });

  describe('encountering identical rows', function () {
    it('does not duplicate rows when a source row points multiple times to the same target', function* () {
      yield arachne(config, 'arachne.epsilon', {id: 1}, ws);

      assert.equal(output[0], 'COPY arachne.delta (id,field) FROM stdin;\n');
      assert.equal(output[1], '1\tdelta1\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.epsilon (id,delta1_id,delta2_id,field) FROM stdin;\n');
      assert.equal(output[4], '1\t1\t1\tepsilon1\n');
      assert.equal(output[5], '\\.\n\n');
    });

    it('does not duplicate rows when source rows from the same table point to the same target row', function* () {
      yield arachne(config, 'arachne.mu', {id: [2, 3]}, ws);

      assert.equal(output[0], 'COPY arachne.lambda (id,field) FROM stdin;\n');
      assert.equal(output[1], '2\tlambda2\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.nu (id,lambda_id,field) FROM stdin;\n');
      assert.equal(output[4], '2\t2\tnu2\n');
      assert.equal(output[5], '\\.\n\n');
      assert.equal(output[6], 'COPY arachne.mu (id,lambda_id,field) FROM stdin;\n');
      assert.equal(output[7], '2\t2\tmu2\n');
      assert.equal(output[8], '3\t2\tmu3\n');
      assert.equal(output[9], '\\.\n\n');
    });

    it('does not duplicate rows when source rows from different tables point to the same target row', function* () {
      yield arachne(config, 'arachne.lambda', {id: 1}, ws);

      assert.equal(output[0], 'COPY arachne.lambda (id,field) FROM stdin;\n');
      assert.equal(output[1], '1\tlambda1\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.nu (id,lambda_id,field) FROM stdin;\n');
      assert.equal(output[4], '1\t1\tnu1\n');
      assert.equal(output[5], '\\.\n\n');
      assert.equal(output[6], 'COPY arachne.mu (id,lambda_id,field) FROM stdin;\n');
      assert.equal(output[7], '1\t1\tmu1\n');
      assert.equal(output[8], '\\.\n\n');
    });
  });

  describe('ordering descendant dependencies', function () {
    it('scripts from the origin', function* () {
      yield arachne(config, 'arachne.omicron', {id: 2}, ws);

      assert.equal(output[0], 'COPY arachne.omicron (id,field) FROM stdin;\n');
      assert.equal(output[1], '2\tomicron2\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.xi (id,omicron_id,field) FROM stdin;\n');
      assert.equal(output[4], '2\t2\txi2\n');
      assert.equal(output[5], '3\t2\txi3\n');
      assert.equal(output[6], '\\.\n\n');
      assert.equal(output[7], 'COPY arachne.pi (id,omicron_id,xi_id,field) FROM stdin;\n');
      assert.equal(output[8], '2\t2\t2\tpi2\n');
      assert.equal(output[9], '3\t2\t3\tpi3\n');
      assert.equal(output[10], '\\.\n\n');
    });

    it('scripts from the first dependent all at once', function* () {
      yield arachne(config, 'arachne.xi', {id: [2, 3]}, ws);

      assert.equal(output[0], 'COPY arachne.omicron (id,field) FROM stdin;\n');
      assert.equal(output[1], '2\tomicron2\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.xi (id,omicron_id,field) FROM stdin;\n');
      assert.equal(output[4], '2\t2\txi2\n');
      assert.equal(output[5], '3\t2\txi3\n');
      assert.equal(output[6], '\\.\n\n');
      assert.equal(output[7], 'COPY arachne.pi (id,omicron_id,xi_id,field) FROM stdin;\n');
      assert.equal(output[8], '2\t2\t2\tpi2\n');
      assert.equal(output[9], '3\t2\t3\tpi3\n');
      assert.equal(output[10], '\\.\n\n');
    });

    it('scripts from a single row of the first dependent', function* () {
      yield arachne(config, 'arachne.xi', {id: 2}, ws);

      assert.equal(output[0], 'COPY arachne.omicron (id,field) FROM stdin;\n');
      assert.equal(output[1], '2\tomicron2\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.xi (id,omicron_id,field) FROM stdin;\n');
      assert.equal(output[4], '2\t2\txi2\n');
      assert.equal(output[5], '3\t2\txi3\n');
      assert.equal(output[6], '\\.\n\n');
      assert.equal(output[7], 'COPY arachne.pi (id,omicron_id,xi_id,field) FROM stdin;\n');
      assert.equal(output[8], '2\t2\t2\tpi2\n');
      assert.equal(output[9], '3\t2\t3\tpi3\n');
      assert.equal(output[10], '\\.\n\n');
    });

    it('scripts from the second dependent all at once', function* () {
      yield arachne(config, 'arachne.pi', {id: [2, 3]}, ws);

      assert.equal(output[0], 'COPY arachne.omicron (id,field) FROM stdin;\n');
      assert.equal(output[1], '2\tomicron2\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.xi (id,omicron_id,field) FROM stdin;\n');
      assert.equal(output[4], '2\t2\txi2\n');
      assert.equal(output[5], '3\t2\txi3\n');
      assert.equal(output[6], '\\.\n\n');
      assert.equal(output[7], 'COPY arachne.pi (id,omicron_id,xi_id,field) FROM stdin;\n');
      assert.equal(output[8], '2\t2\t2\tpi2\n');
      assert.equal(output[9], '3\t2\t3\tpi3\n');
      assert.equal(output[10], '\\.\n\n');
    });

    it('scripts from a single row of the second dependent', function* () {
      yield arachne(config, 'arachne.pi', {id: 2}, ws);

      assert.equal(output[0], 'COPY arachne.omicron (id,field) FROM stdin;\n');
      assert.equal(output[1], '2\tomicron2\n');
      assert.equal(output[2], '\\.\n\n');
      assert.equal(output[3], 'COPY arachne.xi (id,omicron_id,field) FROM stdin;\n');
      assert.equal(output[4], '2\t2\txi2\n');
      assert.equal(output[5], '3\t2\txi3\n');
      assert.equal(output[6], '\\.\n\n');
      assert.equal(output[7], 'COPY arachne.pi (id,omicron_id,xi_id,field) FROM stdin;\n');
      assert.equal(output[8], '2\t2\t2\tpi2\n');
      assert.equal(output[9], '3\t2\t3\tpi3\n');
      assert.equal(output[10], '\\.\n\n');
    });
  });
});
