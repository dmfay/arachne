CREATE SCHEMA arachne;

SET search_path TO arachne;

CREATE TABLE alpha (id INT PRIMARY KEY, field TEXT);

CREATE TABLE beta (id INT PRIMARY KEY, field TEXT);
CREATE TABLE gamma (id INT PRIMARY KEY, beta_id INT, field TEXT);

CREATE TABLE delta (id INT PRIMARY KEY, field TEXT);
CREATE TABLE epsilon (id INT PRIMARY KEY, delta1_id INT, delta2_id INT, field TEXT);

CREATE TABLE zeta (id INT PRIMARY KEY, eta_id INT, field TEXT);
CREATE TABLE eta (id INT PRIMARY KEY, zeta_id INT, field TEXT);

CREATE TABLE theta (id INT PRIMARY KEY, field TEXT);
CREATE TABLE iota (id INT PRIMARY KEY, field TEXT);
CREATE TABLE kappa (theta_id INT, iota_id INT, field TEXT);

CREATE TABLE lambda (id INT PRIMARY KEY, field TEXT);
CREATE TABLE mu (id INT PRIMARY KEY, lambda_id INT, field TEXT);
CREATE TABLE nu (id INT PRIMARY KEY, lambda_id INT, field TEXT);

CREATE TABLE omicron (id INT PRIMARY KEY, field TEXT);
CREATE TABLE xi (id INT PRIMARY KEY, omicron_id INT, field TEXT);
CREATE TABLE pi (id INT PRIMARY KEY, omicron_id INT, xi_id INT, field TEXT);

CREATE TABLE rho (id INT PRIMARY KEY, field TEXT);
CREATE TABLE sigma (rho_id INT, upsilon_id INT, field TEXT);
CREATE TABLE tau (rho_id INT, upsilon_id INT, field TEXT);
CREATE TABLE upsilon (id INT PRIMARY KEY, field TEXT);

CREATE TABLE phi (id INT PRIMARY KEY, phi_id INT, field TEXT);

CREATE TABLE arraytable (id INT, arr TEXT[]);

CREATE TABLE datetable (id INT, value TIMESTAMPTZ, value_array TIMESTAMPTZ[]);

CREATE TYPE myenum AS ENUM ('one', 'two', 'three');
CREATE TABLE enumtable (id INT, value myenum, value_array myenum[]);

ALTER TABLE gamma ADD CONSTRAINT gamma_beta_id_fkey FOREIGN KEY (beta_id) REFERENCES beta(id);
ALTER TABLE epsilon
  ADD CONSTRAINT epsilon_delta1_id_fkey FOREIGN KEY (delta1_id) REFERENCES delta(id),
  ADD CONSTRAINT epsilon_delta2_id_fkey FOREIGN KEY (delta2_id) REFERENCES delta(id);
ALTER TABLE kappa
  ADD PRIMARY KEY (theta_id, iota_id),
  ADD CONSTRAINT kappa_theta_id_fkey FOREIGN KEY (theta_id) REFERENCES theta(id),
  ADD CONSTRAINT kappa_iota_id_fkey FOREIGN KEY (iota_id) REFERENCES iota(id);
ALTER TABLE mu ADD CONSTRAINT mu_lambda_id_fkey FOREIGN KEY (lambda_id) REFERENCES lambda(id);
ALTER TABLE nu ADD CONSTRAINT nu_lambda_id_fkey FOREIGN KEY (lambda_id) REFERENCES lambda(id);
ALTER TABLE xi ADD CONSTRAINT xi_omicron_id_fkey FOREIGN KEY (omicron_id) REFERENCES omicron(id);
ALTER TABLE pi
  ADD CONSTRAINT pi_omicron_id_fkey FOREIGN KEY (omicron_id) REFERENCES omicron(id),
  ADD CONSTRAINT pi_xi_id_fkey FOREIGN KEY (xi_id) REFERENCES xi(id);
ALTER TABLE sigma
  ADD PRIMARY KEY (rho_id, upsilon_id),
  ADD CONSTRAINT sigma_rho_id_fkey FOREIGN KEY (rho_id) REFERENCES rho(id),
  ADD CONSTRAINT sigma_upsilon_id_fkey FOREIGN KEY (upsilon_id) REFERENCES upsilon(id);
ALTER TABLE tau
  ADD PRIMARY KEY (rho_id, upsilon_id),
  ADD CONSTRAINT tau_rho_id_fkey FOREIGN KEY (rho_id) REFERENCES rho(id),
  ADD CONSTRAINT tau_upsilon_id_fkey FOREIGN KEY (upsilon_id) REFERENCES upsilon(id);
ALTER TABLE phi ADD CONSTRAINT phi_phi_id_fkey FOREIGN KEY(phi_id) REFERENCES phi(id);

INSERT INTO alpha VALUES (1, 'alpha1'), (2, 'alpha2');

-- b1-g1; b2-g2; b3-g2
INSERT INTO beta VALUES (1, 'beta1'), (2, 'beta2'), (3, 'beta3');
INSERT INTO gamma VALUES (1, 1, 'gamma1'), (2, 2, 'gamma2'), (3, 2, 'gamma3');

-- d1-(e1, e1); d2-(e2, e3)
INSERT INTO delta VALUES (1, 'delta1'), (2, 'delta2'), (3, 'delta3');
INSERT INTO epsilon VALUES (1, 1, 1, 'epsilon1'), (2, 2, 3, 'epsilon2');

-- z1-e1 impossible loop; e2-z2-e3 false cycle
INSERT INTO zeta VALUES (1, 1, 'zeta1'), (2, 2, 'zeta2'), (3, null, 'zeta3');
INSERT INTO eta VALUES (1, 1, 'eta1'), (2, 3, 'eta2');

ALTER TABLE zeta ADD CONSTRAINT zeta_eta_id_fkey FOREIGN KEY (eta_id) REFERENCES eta(id);
ALTER TABLE eta ADD CONSTRAINT eta_zeta_id_fkey FOREIGN KEY (zeta_id) REFERENCES zeta(id);

-- t1-k-i1; t2-k-(i2, i3); t3
INSERT INTO theta VALUES (1, 'theta1'), (2, 'theta2'), (3, 'theta3');
INSERT INTO iota VALUES (1, 'iota1'), (2, 'iota2'), (3, 'iota3');
INSERT INTO kappa VALUES (1, 1, 'kappa1'), (2, 2, 'kappa2'), (2, 3, 'kappa3');

-- l1-(m1, n1); l2-(m2, m3, n3)
INSERT INTO lambda VALUES (1, 'lambda1'), (2, 'lambda2');
INSERT INTO mu VALUES (1, 1, 'mu1'), (2, 2, 'mu2'), (3, 2, 'mu3');
INSERT INTO nu VALUES (1, 1, 'nu1'), (2, 2, 'nu2');

-- o1-(x1-p1); o2-(x2-p2, x3-p3)
INSERT INTO omicron VALUES (1, 'omicron1'), (2, 'omicron2');
INSERT INTO xi VALUES (1, 1, 'xi1'), (2, 2, 'xi2'), (3, 2, 'xi3');
INSERT INTO pi VALUES (1, 1, 1, 'pi1'), (2, 2, 2, 'pi2'), (3, 2, 3, 'pi3');

-- r1-(s, t)-u1; r2-(s, s, t)-(u2, u3)
INSERT INTO rho VALUES (1, 'rho1'), (2, 'rho2');
INSERT INTO upsilon VALUES (1, 'upsilon1'), (2, 'upsilon2'), (3, 'upsilon3');
INSERT INTO sigma VALUES (1, 1, 'sigma1'), (2, 2, 'sigma2'), (2, 3, 'sigma3');
INSERT INTO tau VALUES (1, 1, 'tau1'), (2, 3, 'tau2');

-- p3-p2-p1
INSERT INTO phi VALUES (1, null, 'phi1'), (2, 1, 'phi2'), (3, 2, 'phi3');

INSERT INTO arraytable VALUES (1, '{one, two}');

INSERT INTO enumtable VALUES (1, 'one', '{one, two, three}');
