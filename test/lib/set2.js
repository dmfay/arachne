'use strict';

const Set2 = require('../../lib/set2');
const assert = require('chai').assert;

describe('Set2', function () {
  describe('constructor', function () {
    it('instantiates a Set2', function () {
      const set = new Set2();

      assert.instanceOf(set, Set2);
      assert.instanceOf(set, Set);
    });

    it('adds elements', function () {
      const set = new Set2([1, 2, 3]);

      assert.equal(set.size, 3);
    });

    describe('filter', function () {
      it('filters elements', function () {
        const set = new Set2([1, 2, 3]).filter(n => n > 1);

        assert.equal(set.size, 2);
      });
    });

    describe('difference', function () {
      it('returns the difference of the set with the argument', function () {
        const set = new Set2([1]).difference(new Set2([1, 2, 3]));

        assert.equal(set.size, 2);
      });
    });

    describe('some', function () {
      it('evaluates whether any elements in the set fulfill the predicate', function () {
        assert.isTrue(new Set2([1, 2, 3]).some(n => n > 1));
        assert.isFalse(new Set2([1, 2, 3]).some(n => n < 1));
      });
    });

    describe('merge', function () {
      it('adds new members', function () {
        const set = new Set2([1]).merge(new Set2([2]));

        assert.equal(set.size, 2);
      });

      it('keeps existing members', function () {
        const set = new Set2([1]).merge(new Set2([1]));

        assert.equal(set.size, 1);
      });

      it('uses custom comparators', function () {
        const one = new Set2([{id: 1, val: 'one'}]);
        const two = new Set2([{id: 1, val: 'two'}]);

        const set = one.merge(two, (a, b) => a.id === b.id);

        assert.equal(set.size, 1);
      });

      it('merges member array fields', function () {
        const one = new Set2([{id: 1, arr: ['a', 'b']}]);
        const two = new Set2([{id: 1, arr: ['b', 'c']}]);

        const set = one.merge(two, (a, b) => a.id === b.id);

        assert.equal(set.size, 1);
        assert.sameMembers([...set][0].arr, ['a', 'b', 'c']);
      });

      it('consolidates multiple overlaps in the origin set', function () {
        const one = new Set2([{id: 1, arr: ['a', 'b']}, {id: 1, arr: ['c', 'd']}]);
        const two = new Set2([{id: 1, arr: ['b', 'c']}]);

        const set = one.merge(two, (a, b) => a.id === b.id);

        assert.equal(set.size, 1);
        assert.sameMembers([...set][0].arr, ['a', 'b', 'c', 'd']);
      });

      it.skip('consolidates multiple overlaps in the new set', function () {
        const one = new Set2([{id: 1, arr: ['a', 'b']}]);
        const two = new Set2([{id: 1, arr: ['b', 'c']}, {id: 1, arr: ['a', 'd']}]);

        const set = one.merge(two, (a, b) => a.id === b.id);

        assert.equal(set.size, 1);
        assert.sameMembers([...set][0].arr, ['a', 'b', 'c', 'd']);
      });
    });
  });
});
