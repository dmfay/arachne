'use strict';

const compareEdges = require('../../lib/compare-edges');
const assert = require('chai').assert;

describe('compareEdges', function () {
  describe('comparing table properties', function () {
    it('returns false if sources and destinations are different', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: []},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: []}
      };
      const b = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: []},
        target_table: 'three',
        target_column: 'id',
        target_condition: {id: []}
      };

      assert.isFalse(compareEdges(a, b));
    });

    it('returns true if sources and destinations match', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: []},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: []}
      };
      const b = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: []},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: []}
      };

      assert.isFalse(compareEdges(a, b));
    });

    it('returns true if sources and destinations match in reverse', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: []},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: []}
      };
      const b = {
        source_table: 'two',
        source_column: 'id',
        source_condition: {id: []},
        target_table: 'one',
        target_column: 'id',
        target_condition: {id: []}
      };

      assert.isFalse(compareEdges(a, b));
    });

    it('compares keys of object properties', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: []},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: []}
      };
      const b = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: []},
        target_table: 'two',
        target_column: 'id',
        target_condition: {otherfield: []}
      };

      assert.isFalse(compareEdges(a, b));
    });
  });

  describe('comparing conditions', function () {
    it('returns false if an end has a condition mismatch', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [1, 2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [3, 4]}
      };
      const b = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [1, 2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [5, 6]}
      };

      assert.isFalse(compareEdges(a, b));
    });

    it('returns true if conditions overlap without strict', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [1, 2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [3, 4]}
      };
      const b = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [4]}
      };

      assert.isTrue(compareEdges(a, b));
    });

    it('returns true if reversed edge conditions overlap without strict', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [1, 2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [3, 4]}
      };
      const b = {
        source_table: 'two',
        source_column: 'id',
        source_condition: {id: [4]},
        target_table: 'one',
        target_column: 'id',
        target_condition: {id: [2]}
      };

      assert.isTrue(compareEdges(a, b));
    });

    it('returns false if conditions overlap inexactly with strict', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [1, 2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [3, 4]}
      };
      const b = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [4]}
      };

      assert.isFalse(compareEdges(a, b, true));
    });

    it('returns true if conditions overlap exactly with strict', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [1, 2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [3, 4]}
      };
      const b = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [1, 2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [3, 4]}
      };

      assert.isTrue(compareEdges(a, b, true));
    });

    it('returns true if reversed edge conditions overlap exactly with strict', function () {
      const a = {
        source_table: 'one',
        source_column: 'id',
        source_condition: {id: [1, 2]},
        target_table: 'two',
        target_column: 'id',
        target_condition: {id: [3, 4]}
      };
      const b = {
        source_table: 'two',
        source_column: 'id',
        source_condition: {id: [3, 4]},
        target_table: 'one',
        target_column: 'id',
        target_condition: {id: [1, 2]}
      };

      assert.isTrue(compareEdges(a, b, true));
    });
  });
});
