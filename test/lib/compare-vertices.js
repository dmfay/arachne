'use strict';

const compareVertices = require('../../lib/compare-vertices');
const assert = require('chai').assert;

describe('compareVertices', function () {
  it('returns false if tables differ', function () {
    assert.isFalse(compareVertices({table: 'one'}, {table: 'two'}));
  });

  it('returns false if condition keysets differ', function () {
    const a = {table: 'one', condition: {id1: []}};
    const b = {table: 'one', condition: {id2: []}};

    assert.isFalse(compareVertices(a, b));
  });

  it('returns false if conditions are different', function () {
    const a = {table: 'one', condition: {id: [1, 2, 3]}};
    const b = {table: 'one', condition: {id: [4, 5, 6]}};

    assert.isFalse(compareVertices.apply({edges: []}, [a, b]));
  });

  it('returns true if conditions overlap', function () {
    const a = {table: 'one', condition: {id: [1, 2, 3]}};
    const b = {table: 'one', condition: {id: [3, 4, 5]}};

    assert.isTrue(compareVertices(a, b));
  });
});
