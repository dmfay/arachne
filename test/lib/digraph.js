'use strict';

const Digraph = require('../../lib/digraph');
const Set2 = require('../../lib/set2');
const assert = require('chai').assert;

describe('Digraph', function () {
  describe('constructor', function () {
    it('returns a Digraph', function () {
      const graph = new Digraph();

      assert.instanceOf(graph.vertices, Set2);
      assert.instanceOf(graph.edges, Set2);
      assert.equal(graph.vertices.size, 0);
      assert.equal(graph.edges.size, 0);
    });

    it('instantiates vertices and edges', function () {
      const graph = new Digraph(['a', 'b'], ['ab']);

      assert.instanceOf(graph.vertices, Set2);
      assert.instanceOf(graph.edges, Set2);
      assert.equal(graph.vertices.size, 2);
      assert.equal(graph.edges.size, 1);
    });
  });

  describe('merge', function () {
    it('adds new vertices and edges', function () {
      const graph1 = new Digraph();
      const graph2 = new Digraph(['a', 'b'], ['ab']);

      graph1.merge(graph2);

      assert.equal(graph1.vertices.size, 2);
      assert.equal(graph1.edges.size, 1);
    });

    it('merges matching vertices and edges', function () {
      const graph1 = new Digraph(['a', 'b'], ['ab']);
      const graph2 = new Digraph(['a', 'b', 'c'], ['ab', 'bc']);

      graph1.merge(graph2);

      assert.equal(graph1.vertices.size, 3);
      assert.equal(graph1.edges.size, 2);
    });
  });
});
