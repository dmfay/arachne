'use strict';

const toposort = require('../../lib/toposort');
const Digraph = require('../../lib/digraph');
const assert = require('chai').assert;

describe('toposort', function () {
  it('is a generator', function () {
    assert.instanceOf(toposort, (function*(){}).constructor);
  });

  it('yields a subgraph of independent vertices and targeting edges', function () {
    const graph = new Digraph([{
      table: 'one', condition: { id: [1, 2] }
    }, {
      table: 'two', condition: { id: [3, 4] }
    }, {
      table: 'three', condition: { id: [5, 6] }
    }], [{
      source_table: 'two',
      source_condition: { id: [3, 4] },
      target_table: 'one',
      target_condition: { id: [1, 2] }
    }, {
      source_table: 'two',
      source_condition: { id: [3, 4] },
      target_table: 'three',
      target_condition: { id: [5, 6] }
    }]);

    const subgraph = toposort(graph).next().value;

    assert.equal(subgraph.vertices.size, 2);
    assert.equal(subgraph.edges.size, 2);
    assert.isTrue(subgraph.vertices.some(v => v.table === 'one'));
    assert.isTrue(subgraph.vertices.some(v => v.table === 'three'));
  });

  it('yields an empty subgraph if the digraph is cyclic', function () {
    const graph = new Digraph([{
      table: 'one', condition: { id: [1, 2] }
    }, {
      table: 'two', condition: { id: [3, 4] }
    }], [{
      source_table: 'two',
      source_condition: { id: [3, 4] },
      target_table: 'one',
      target_condition: { id: [1, 2] }
    }, {
      source_table: 'one',
      source_condition: { id: [1, 2] },
      target_table: 'two',
      target_condition: { id: [3, 4] }
    }]);

    const subgraph = toposort(graph).next().value;

    assert.equal(subgraph.vertices.size, 0);
    assert.equal(subgraph.edges.size, 0);
  });
});
