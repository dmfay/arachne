'use strict';

const conditionsOverlap = require('../../lib/conditions-overlap');
const assert = require('chai').assert;

describe('conditionsOverlap', function () {
  describe('without strict', function () {
    it('returns true when all conditions intersect', function () {
      const a = {
        one: [1, 2],
        two: [3, 4]
      };
      const b = {
        one: [2, 3],
        two: [4, 5]
      };

      assert.isTrue(conditionsOverlap(a, b));
    });

    it('returns false if any condition does not intersect', function () {
      const a = {
        one: [1, 2],
        two: [3, 4]
      };
      const b = {
        one: [2, 3],
        two: [5, 6]
      };

      assert.isFalse(conditionsOverlap(a, b));
    });

    it('returns false if the keys do not match', function () {
      const a = {
        one: [1, 2],
        two: [3, 4]
      };
      const b = {
        one: [2, 3]
      };

      assert.isFalse(conditionsOverlap(a, b));
    });
  });

  describe('with strict', function () {
    it('returns true when all conditions are identical', function () {
      const a = {
        one: [1, 2],
        two: [3, 4]
      };
      const b = {
        one: [1, 2],
        two: [3, 4]
      };

      assert.isTrue(conditionsOverlap(a, b, true));
    });

    it('returns false if any conditions are not identical', function () {
      const a = {
        one: [1, 2],
        two: [3, 4]
      };
      const b = {
        one: [1, 2],
        two: [3]
      };

      assert.isFalse(conditionsOverlap(a, b, true));
    });

    it('returns false if the keys do not match', function () {
      const a = {
        one: [1, 2],
        two: [3, 4]
      };
      const b = {
        one: [2, 3]
      };

      assert.isFalse(conditionsOverlap(a, b, true));
    });
  });
});
